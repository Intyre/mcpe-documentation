MCPE versions and checksum
**************************

0.10.0.5
========
::

	 Created    Mon Nov  3 17:01:00 2014
	 MD5        e664373bb384f75a6983dc20db2bd9f9
	 SHA1       10d67f5d8c022afe07054668b0e3c001eccce725
	 
0.10.0.4
========
::

	Created    Fri Oct 31 16:07:00 2014
	MD5        eaa3b412683e24fb895e662ce082f551
	SHA1       0009cff88459c7150ac2b8883081f717fe301d73

0.10.0.3
========
::

	Created    Mon Oct 27 12:28:54 2014
	MD5        3ce22a936fed35ae1a0f0d18b1cf30a0
	SHA1       8844f3abf52e404852e36083b79e9c556418bd40

0.10.0.2
========
::

	Created    Thu Oct 23 16:23:04 2014
	MD5        6b0a593b307a31709d688c98b0abea8a
	SHA1       ab082d1f8129b465248e6533acdcdb7b971c5b6e

0.10.0.1
========
::

	Created    Fri Oct 17 17:39:26 2014
	MD5        7e90b774fb77d4e280954af8abc37b1a
	SHA1       6634935b76aaee970703e4529d94dbf012fa9413

0.9.5.0
=======
::

	Created    Thu Jul 24 09:03:14 2014
	MD5        75837027a0538163140a22575321d24e
	SHA1       46da1bbf5eecc3dc14ef5c31e3f550f7b210d521

0.9.4.0
=======
::

	Created    Thu Jul 17 16:49:42 2014
	MD5        4cd41b6b72b6f0de9371703c052f3de4
	SHA1       332ad02500022b6ee6fd361401013d7834aa9f6a

0.9.3.0
=======
::

	Created    Wed Jul 16 18:22:52 2014
	MD5        c6101921c9e7ba91e6c7acc5bbda0041
	SHA1       c8580076da0f51b9d36ee5bb9f576e1a6406cd0a

0.9.2.0
=======
::

	Created    Tue Jul 15 17:53:50 2014
	MD5        cc21de8984e69aec8198d23556656bfd
	SHA1       dd8b45581558881742da38835de3e245e28d65b3

0.9.1.0
=======
::

	Created    Fri Jul 11 18:13:08 2014
	MD5        3c32bcbf7fcedc98716935854be10316
	SHA1       02fcc3721770e00ef3129d4ebd1c711027b16a38

0.9.0.12
========
::

	Created    Mon Jun 30 20:01:52 2014
	MD5        fddc0ba33dd5a2d81ee7564a6d2cc27f
	SHA1       47c9122fb0dee1f3f2810f7dc99b28c302624b78

0.9.0.11
========
::

	Created    Fri Jun 27 16:59:24 2014
	MD5        b6dc75502ef39ded984159c6fce1fd34
	SHA1       220e049aa7707e1fc7b7a67dbac67c8dfd005e10

0.9.0.8
=======
::

	Created    Tue Jun 24 15:57:38 2014
	MD5        85c28c17fc1c66332e0be43f4fbaf86c
	SHA1       8f01b0aa56d5e701cb5cfa19c8d7b86449f316d4

0.9.0.7
=======
::

	Created    Mon Jun 23 18:22:58 2014
	MD5        c334c07c2f56acd76ca81323e6d6ad18
	SHA1       074a48819561b5df31ea1867a839ffa35c29b7b3

0.9.0.6
=======
::

	Created    Thu Jun 19 17:13:46 2014
	MD5        15ce2d41a75377a9ed21fc9d08c515f1
	SHA1       8935411d9846b3a0c804aaf328b47f2e60ddb702

0.9.0.4
=======
::

	Created    Fri Jun 13 17:27:20 2014
	MD5        646b98e594aea40d7790980331ac7a7b
	SHA1       698ae67c165f5f5b1862cc3d45190e1b67f0e9fd

0.9.0.3
=======
::

	Created    Wed Jun 11 16:01:34 2014
	MD5        0911cec8344b68c11941e949147e7402
	SHA1       34ea8baae4053e66504be1b74694439f713056d9

0.9.0.2
=======
::

	Created    Tue Jun 10 17:29:00 2014
	MD5        09b1648c7143025447dcdf51db5e3655
	SHA1       0dfa15e0dcb05ba9752a79a6fda7462ae3984c10

0.9.0.1
=======
::

	Created    Mon Jun  9 16:53:28 2014
	MD5        12b0d557d11a48c5efaf33d5f801806a
	SHA1       cc7bd2702157db86500119bcca595ac1135eef48

0.9.0.0
=======
::

	Created    Thu Jul  3 11:34:20 2014
	MD5        7398b69bb9f1dc3372c824a92a2b6eb9
	SHA1       a6612e6e103798e05318ac2a33f31f33a6e7aada

0.8.1.0
=======
::

	Created    Wed Dec 18 09:29:10 2013
	MD5        0557c2d3b7e0d68b20c1726cbb322c10
	SHA1       c30bd6e3b72be7b97a222d79d1405b55f6d3bf71

0.7.6.0
=======
::

	Created    Fri Oct 11 12:36:28 2013
	MD5        063c1cc8af7a0076cba0b4b009dd392a
	SHA1       66e2d9df8b38c603c58b700a14a1f71cf0be5cdb

0.7.5.0
=======
::

	Created    Wed Sep  4 11:53:16 2013
	MD5        9f646e1cf57f69b6a76b94dfc719c211
	SHA1       ba40cd1f639c9e74f518991a0f2b3cf0075a083a

0.7.4.0
=======
::

	Created    Fri Aug 30 14:03:30 2013
	MD5        ce4c31c82c32cfc48835d471e7e33863
	SHA1       d1f31fc901ba4df188f561c0a62c11fde72e9a0b

0.7.3.0
=======
::

	Created    Thu Aug 15 10:29:38 2013
	MD5        161bc70c33099e6cce897892120a45c8
	SHA1       f267d557d090966dc0f6e2f7e084551d2dc49575

0.7.2.0
=======
::

	Created    Thu Jul  4 16:36:46 2013
	MD5        101cfaa012269ddcffba7a54bbbdafc6
	SHA1       f1bbf7f318466288ef523a4d13aac35ea0316c3b

0.7.1.0
=======
::

	Created    Fri Jun  7 17:10:34 2013
	MD5        4372dc188d3e3b98d756a887e27b2c50
	SHA1       9beada802e839c6cd8deadde526b2a7793349a28

0.7.0.0
=======
::

	Created    Wed Jun  5 19:34:58 2013
	MD5        2eba5efa75f6fabd34f2d6c2f4017681
	SHA1       fe0b63b8fd1a2941ae8f6b2e2d2c5377a2097425

0.6.1.0
=======
::

	Created    Thu Jan 31 20:35:42 2013
	MD5        f5f7200651d75f03fe202d051ccd31c4
	SHA1       dfaab7a254a19b32fc378c72a5505916069a2a17

0.6.0.0
=======
::

	Created    Wed Jan 30 13:57:18 2013
	MD5        a3859b9c9c1f49f8a45d03e058b3bee8
	SHA1       8ba82f69d874af6d2261a7db1d07d3fb4446287c

0.5.0.0
=======
::

	Created    Thu Nov 15 12:57:52 2012
	MD5        ea07fdf5f03db8efdd3dc505ad60cda9
	SHA1       30e67fd081e9557139b8925bd620f367e47870e7

0.4.0.0
=======
::

	Created    Thu Sep  6 11:00:28 2012
	MD5        27e2f8aa62e60f5544e296ae95635632
	SHA1       3a1dff2666bdca184344a5a4446f15e1c61b17cd

0.3.3.0
=======
::

	Created    Thu Aug  9 18:03:00 2012
	MD5        9e9977b94b101d147f9f1c462b696908
	SHA1       897dca685a9b40fff88bd9882024ebeefc64fda4

0.3.2.0
=======
::

	Created    Tue Jul 17 08:20:06 2012
	MD5        a5433123c90d004843c51a82e28bdc0a
	SHA1       2e53d32e10dc3b82414c1d7acf6f937d8d61eb14

0.3.0.0
=======
::

	Created    Wed Apr 25 01:46:34 2012
	MD5        eaa30074734cbc4c39ac1cc6966c1cbc
	SHA1       b071ea1957a6cec09dc16588a784f7561c1ec013

0.2.2.0
=======
::

	Created    Sun Mar 18 07:50:12 2012
	MD5        8b067368f5b109d40466b508bb6afc7b
	SHA1       78af7bf3569eb6dc4c89ca4e50db2b9198933856

0.2.1.0
=======
::

	Created    Thu Mar 15 01:06:48 2012
	MD5        464ac369887229944630488639a7a174
	SHA1       0b48db30c0e955573906a63e710450b071a4a300

0.2.0.0
=======
::

	Created    Sat Feb 11 12:20:04 2012
	MD5        626e60ae67cfb2816b1b0d87c0a4190b
	SHA1       16bb2190f656d24370cb45c1a33c2253e14c264c

0.1.3.0
=======
::

	Created    Fri Feb 29 03:33:46 2008
	MD5        1aa5b92852ea699c1e9c3826aebc32e4
	SHA1       702e585663dd0fd4c8ed564f981f7beb9ce3a30f

0.1.2.0
=======
::

	Created    Thu Oct 13 21:45:14 2011
	MD5        eea48294ca763591e0ea6096a1a48948
	SHA1       523ff7962b984322dd7d71788fb0ff931c5c4e64

0.1.1.0
=======
::

	Created    Fri Oct  7 21:35:54 2011
	MD5        bdd26f376a24f77a6020f1727929e413
	SHA1       7b874c3afc913ea97c062c3e18ae2e56dd735722

0.1.0.0
=======
::

	Created    Fri Aug 19 10:13:46 2011
	MD5        6a33f66f6f1d2655feee70646fda8353
	SHA1       e0273784f3c75a398c7b0a50f18ad4414baedeb8

